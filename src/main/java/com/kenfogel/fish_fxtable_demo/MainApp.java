package com.kenfogel.fish_fxtable_demo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fish_fxtable_demo.persistence.FishDAO;
import com.kenfogel.fish_fxtable_demo.persistence.FishDAOImpl;
import com.kenfogel.fish_fxtable_demo.view.FishFXTableController;
import java.io.IOException;
import java.sql.SQLException;

public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Stage primaryStage;
    private AnchorPane fishFXTableLayout;
    private final FishDAO fishDAO;

    public MainApp() {
        super();
        fishDAO = new FishDAOImpl();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("The Fish Table");

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/bluefish_icon.png")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * Load the layout and controller.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/fxml/FishFXTableLayout.fxml"));
            fishFXTableLayout = (AnchorPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(fishFXTableLayout);
            primaryStage.setScene(scene);

            FishFXTableController controller = loader.getController();
            controller.setFishDAO(fishDAO);
            controller.displayTheTable();
        } catch (IOException | SQLException e) {
            LOG.error("Error display table", e);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
}
